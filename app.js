var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var crypto = require('crypto');
var sha256 = require('sha256');
var eccrypto = require("eccrypto");
var ECKey = require('eckey');
var conv = require('binstring');



var cryptoLib = require('cryptlib');





var aesjs = require('aes-js');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();


//AesUtil = new AesUtil(128, 10);


var alice = crypto.createECDH('secp256k1');


var bob = crypto.createECDH('secp256k1');

var sahitya = sha256('batman');

console.log("Sahity hash " + sahitya);

alice.setPrivateKey(
  crypto.createHash('sha256').update('batman', 'utf8').digest()
);

console.log("Message hash creted ------- " + crypto.createHash('sha256').update('batman', 'utf8').digest().toString('hex'));


// Bob uses a newly generated cryptographically strong
// pseudorandom key pair
bob.generateKeys();


console.log("Alice Private Key " + alice.getPrivateKey().toString('hex') + "\n Alice Public Key " + alice.getPublicKey().toString('hex'));

var private_key = alice.getPrivateKey().toString('hex');

var public_key = alice.getPublicKey().toString('hex');



var plainText = "this is my plain text";
var key = "simplekey";
var iv = "1234123412341234";


shaKey = cryptoLib.getHashSha256(key, 32); // This line is not needed on Android or iOS. Its already built into CryptLib.m and CryptLib.java

var encryptedString = cryptoLib.encrypt(plainText, shaKey, iv);
console.log('encryptedString %s', encryptedString);

var decryptedString = cryptoLib.decrypt(encryptedString, shaKey, iv);
console.log('decryptedString %s', decryptedString);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



// function sha256(msg) {
//   return crypto.createHash("sha256").update(msg).digest("hex");
// }

// var keySize = 128;
// var iterationCount = 10;



// function generateKey(salt, passPhrase) {
//   var key = CryptoJS.PBKDF2(
//     passPhrase,
//     CryptoJS.enc.Hex.parse(salt), {
//       keySize: keySize,
//       iterations: iterationCount
//     });
//   return key;
// }


// function encrypted_string_from_aes(salt, iv, passPhrase, plainText) {
//   var key = generateKey(salt, passPhrase);
//   var encrypted = CryptoJS.AES.encrypt(
//     plainText,
//     key, {
//       iv: CryptoJS.enc.Hex.parse(iv)
//     });
//   return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
// }


// function decrypted_string_from_aes(salt, iv, passPhrase, cipherText) {
//   var key = generateKey(salt, passPhrase);
//   var cipherParams = CryptoJS.lib.CipherParams.create({
//     ciphertext: CryptoJS.enc.Base64.parse(cipherText)
//   });
//   var decrypted = CryptoJS.AES.decrypt(
//     cipherParams,
//     key, {
//       iv: CryptoJS.enc.Hex.parse(iv)
//     });
//   return decrypted.toString(CryptoJS.enc.Utf8);
// }



module.exports = app;